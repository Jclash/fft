oldnp=16 oldrank=6 np=8 rank=6
FFT test (fftw)
N x N = 35000 x 35000
Overall memory allocated for one array: 18692 Mb
Memory allocated on this core: 2336.5 Mb
Forward DFT...One Distributed Forward 2d DFT time: 23.2823s
FLOPs for one forward DFT: 8.218e+10 adds, 4.242e+10 mults, 4.27e+10 fmas
