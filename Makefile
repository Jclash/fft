include MakeCompiler.mk

SRCPATH=src
OBJPATH=bin
BINPATH=bin
PPROCPATH=postprocessing

BINS=$(BINPATH)/run
OBJS=$(OBJPATH)/main.o
SOURCES=$(SRCPATH)/main.cpp
HEADERS=

include MakePackages.mk

all: $(BINS)

$(BINS): $(OBJS)  
	$(CC) $(LIBS) $(OBJS) -lfftw3_mpi -lfftw3 -lpthread -lrt -lm -o $(BINPATH)/run
	./start

$(OBJPATH)/main.o:  $(SRCPATH)/main.cpp 
	$(CC) -O2 $(INCLUDES) -c  $(SRCPATH)/main.cpp -o $(OBJPATH)/main.o


#$(OBJS): $(SOURCES) $(HEADERS)
#	$(CC) -O2 $(INCLUDES) -c $(SOURCES) 
	
#*.o: *.cpp *.h
#	$(CC) -O2 $(INCLUDES) -c *.cpp

gpu: $(BINPATH)/run_gpu

$(BINPATH)/run_gpu: $(SRCPATH)/main_gpu.cu
	nvcc $(SRCPATH)/main_gpu.cu  --gpu-architecture=compute_35 --gpu-code=sm_37 -lcufft -o $(BINPATH)/run_gpu

mainfig: $(PPROCPATH)/fig1.png

$(PPROCPATH)/fig1.png:
	make -C $(PPROCPATH)
	

clean:
	rm -f *.o
	rm -f $(BINS)
	rm -f $(OBJS)
	rm -f err
	rm -f $(BINPATH)/run_gpu
	
clean_out:
	rm -f *.bmp
	rm -f *.m
	rm -f topology_init.cif
	rm -f topology_work.cif
	rm -f machines.*
	rm -f ./run.*/*.*
	rm -f ./run.*/*
	rm -f ./run.*/.hosts
	rm -rfd run.*
	
cleanall: clean clean_out	


