#!/bin/bash
export CPATH="/opt/intel/composerxe-2011.3.174/mkl/include"
export CVS_RSH="ssh"
export FPATH="/opt/intel/composerxe-2011.3.174/mkl/include"
export G_BROKEN_FILENAMES="1"
export HISTSIZE="1000"
export HOME="/nethome/knyazkov"
export HOSTNAME="mvs100k.jscc.ru"
export INCLUDE="/opt/intel/composerxe-2011.3.174/mkl/include"
export INPUTRC="/etc/inputrc"
export INTEL_LICENSE_FILE="/opt/intel/composerxe-2011.3.174/licenses:/opt/intel/licenses:/nethome/knyazkov/intel/licenses"
export LANG="en_US.UTF-8"
export LC_ADDRESS="en_SG.UTF-8"
export LC_IDENTIFICATION="en_SG.UTF-8"
export LC_MEASUREMENT="en_SG.UTF-8"
export LC_MONETARY="en_SG.UTF-8"
export LC_NAME="en_SG.UTF-8"
export LC_NUMERIC="en_SG.UTF-8"
export LC_PAPER="en_SG.UTF-8"
export LC_TELEPHONE="en_SG.UTF-8"
export LC_TIME="en_SG.UTF-8"
export LD_LIBRARY_PATH="/nethome/knyazkov/lib:/opt/intel/composerxe-2011.3.174/compiler/lib/intel64:/opt/intel/composerxe-2011.3.174/compiler/lib/intel64:/opt/intel/composerxe-2011.3.174/mkl/lib/intel64:/opt/intel/composerxe-2011.3.174/mpirt/lib/intel64"
export LESSOPEN="|/usr/bin/lesspipe.sh %s"
export LIBRARY_PATH="/opt/intel/composerxe-2011.3.174/compiler/lib/intel64:/opt/intel/composerxe-2011.3.174/compiler/lib/intel64:/opt/intel/composerxe-2011.3.174/mkl/lib/intel64"
export LOGNAME="knyazkov"
export LS_COLORS="no=00:fi=00:di=00;34:ln=00;36:pi=40;33:so=00;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:ex=00;32:*.cmd=00;32:*.exe=00;32:*.com=00;32:*.btm=00;32:*.bat=00;32:*.sh=00;32:*.csh=00;32:*.tar=00;31:*.tgz=00;31:*.arj=00;31:*.taz=00;31:*.lzh=00;31:*.zip=00;31:*.z=00;31:*.Z=00;31:*.gz=00;31:*.bz2=00;31:*.bz=00;31:*.tz=00;31:*.rpm=00;31:*.cpio=00;31:*.jpg=00;35:*.gif=00;35:*.bmp=00;35:*.xbm=00;35:*.xpm=00;35:*.png=00;35:*.tif=00;35:"
export MAIL="/var/spool/mail/knyazkov"
export MANPATH="/opt/intel/composerxe-2011.3.174/man/en_US:/opt/intel/composerxe-2011.3.174/man/en_US::/common/mvapich/man:"
export MC_TMPDIR="/tmp/mc-knyazkov"
export MKLROOT="/opt/intel/composerxe-2011.3.174/mkl"
export NLSPATH="/opt/intel/composerxe-2011.3.174/compiler/lib/intel64/locale/%l_%t/%N:/opt/intel/composerxe-2011.3.174/mkl/lib/intel64/locale/%l_%t/%N"
export OLDPWD
export PATH="/opt/intel/composerxe-2011.3.174/bin/intel64:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/common/runmvs/bin:/common/mvapich/bin/:/opt/intel/composerxe-2011.3.174/mpirt/bin/intel64"
export PWD="/home4/pstorage1/ipmex4/work/fft"
export SHELL="/bin/sh"
export SHLVL="4"
export SSH_ASKPASS="/usr/libexec/openssh/gnome-ssh-askpass"
export SSH_CLIENT="188.44.34.94 34918 22"
export SSH_CONNECTION="188.44.34.94 34918 83.149.214.100 22"
export SSH_TTY="/dev/pts/14"
export TERM="xterm"
export USER="knyazkov"
EnvCommandLine=(
)
num_nodes=1
hosts_list="$2"
exitstatus_file="$3"
prefix="$5"
task_image="$4"
np=8
tview_flag=
progname="$5 /home4/pstorage1/ipmex4/work/fft/./bin/run"
mpich_args=""
cmdLineArgs=" 10000 10000 0"
width_distribution=0
numofnodes=1
proc_per_node=


MPIRUN_HOME=/common/mvapich/bin
cd "/home4/pstorage1/ipmex4/work/fft"

if [  "" ] ;then
  machine_file=`mktemp /tmp/machinefile.XXXXXX` || exit 1
  /common/runmvs/bin/confread $task_image Machinefile > $machine_file 
  gentmpname=`mktemp /tmp/machines.XXXXXX` || exit 1
  /common/runmvs/bin/genmachinefile "$hosts_list" $machine_file 8 >$gentmpname
  hosts_list=$gentmpname
  rm -f $machine_file
fi

trap ' if [ "" ] ; then   rm $gentmpname ;fi ; exit '  1 2 3 15



EnvCommandLine=(  "${EnvCommandLine[@]}" MACHINEFILE_NAME=$hosts_list ) 
    export LMUTIL=lmutil


if [ x = x-totalview ]
then
    if ! check_license -c /common/totalview/license.dat -f TV/Base
    then
        sleep 1
        echo 215 >$exitstatus_file
        exit 215
    fi
fi
(

. /common/runmvs/bin/run-mvapich-ib

)
exitstatus=$?

if [ 0 -ne 0 ] ; then
    exitstatus=215
fi


if [ "" ] ; then   rm $gentmpname ;fi 
echo $exitstatus >$exitstatus_file
exit $exitstatus
