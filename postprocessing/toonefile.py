
from numpy import *
import os

print "Yo!"

directory = "./"

a1 = 0
N = 0
t = 0.0

with open("allres-hybrilit.txt", "w") as outf:
  for filename in os.listdir(directory):
    if filename.endswith(".out"):
      print(os.path.join(directory, filename))
      with open(filename, 'r') as f:
        for y in [x.rstrip() for x in f. readlines()]:
          if y[:8] == "N x N = ":
            global N, a1
            endy = y[7:]
            strN = endy[:endy.find("x")]
            N = eval(strN)
            print y
            print endy
            print N
            if a1 == 0:
              a1 = 1
          if (y[:24] == "One Forward 2d DFT time:") or (y[:36] == "One Distributed Forward 2d DFT time:"):
            global t, a1
            endy = y[y.find("time:")+5:]
            strN = endy[:endy.find("s")]
            t = eval(strN)
            print y
            print endy
            print t
            if a1 == 1:
              a1 = 2

          if a1 == 2:
            global a1
            outf.write( "{0:d} {1:f}\n".format(N, t) )
            a1 = 0
      continue
    else:
      continue


with open("allres-mvs100k.txt", "w") as outf:
  for filename in os.listdir(directory):
    if filename.startswith("output-mvs100k"):
      print(os.path.join(directory, filename))
      with open(filename, 'r') as f:
        for y in [x.rstrip() for x in f. readlines()]:
          if y[:8] == "N x N = ":
            global N, a1
            endy = y[7:]
            strN = endy[:endy.find("x")]
            N = eval(strN)
            print y
            print endy
            print N
            if a1 == 0:
              a1 = 1
          if (y[:24] == "One Forward 2d DFT time:") or (y[:36] == "One Distributed Forward 2d DFT time:"):
            global t, a1
            endy = y[y.find("time:")+5:]
            strN = endy[:endy.find("s")]
            t = eval(strN)
            print y
            print endy
            print t
            if a1 == 1:
              a1 = 2

          if a1 == 2:
            global a1
            outf.write( "{0:d} {1:f}\n".format(N, t) )
            a1 = 0
      continue
    else:
      continue

with open("allres-mvs10p.txt", "w") as outf:
  for filename in os.listdir(directory):
    if filename.startswith("output-mvs10p"):
      print(os.path.join(directory, filename))
      with open(filename, 'r') as f:
        for y in [x.rstrip() for x in f. readlines()]:
          if y[:8] == "N x N = ":
            global N, a1
            endy = y[7:]
            strN = endy[:endy.find("x")]
            N = eval(strN)
            print y
            print endy
            print N
            if a1 == 0:
              a1 = 1
          if (y[:24] == "One Forward 2d DFT time:") or (y[:36] == "One Distributed Forward 2d DFT time:"):
            global t, a1
            endy = y[y.find("time:")+5:]
            strN = endy[:endy.find("s")]
            t = eval(strN)
            print y
            print endy
            print t
            if a1 == 1:
              a1 = 2

          if a1 == 2:
            global a1
            outf.write( "{0:d} {1:f}\n".format(N, t) )
            a1 = 0
      continue
    else:
      continue
