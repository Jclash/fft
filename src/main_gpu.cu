// example1.cpp : Defines the entry point for the console application.
//
 
//#include <stdafx.h>
  
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <iostream>

#include "time.h"
#include <sys/time.h>
#include <sys/resource.h>

#include "cufft.h"

#define NX 256
#define NY 256
#define NRANK 2
#define BATCH 1
   
// Kernel that executes on the CUDA device
__global__ void square_array(double *a, int N){

  int idx = blockIdx.x * blockDim.x + threadIdx.x;

  if (idx<N) a[idx] = a[idx] * a[idx];
}

double diffclock(double clock1,double clock2)
{
  //return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
    //return ((clock2-clock1)*1000.0)/(double)CLOCKS_PER_SEC;
      return clock2-clock1;
      }
      
      double mclock(){
        timeval tim;
          gettimeofday(&tim, NULL);
            return tim.tv_sec+(tim.tv_usec/1000000.0);
            }
        
// main routine that executes on the host
int CUDA_dataCopy_testTime( long int NN ){
  using namespace std;
  
  cout<<"start of host program"<<endl;

  double *a_h, *a_d;  // Pointer to host & device arrays
  const long int N = NN*NN;  // Number of elements in arrays
  size_t size = N * sizeof(double);

  cout<<"N = "<<NN<<endl;

  a_h = (double *)malloc(size);        // Allocate array on host
  cudaError_t status = cudaMallocHost( (void**)&a_h, size );
  if (status != cudaSuccess)
    printf("Error allocating pinned host memoryn");
  
  cudaMalloc((void **) &a_d, size);   // Allocate array on device

  // Initialize host array and copy it to CUDA device
  for (int i=0; i<N; i++) a_h[i] = (double)i;
  
  double aclock1,aclock2;
  cudaDeviceSynchronize();
  aclock1 = mclock();
  cudaMemcpy(a_d, a_h, size, cudaMemcpyHostToDevice);
  cudaDeviceSynchronize();
  aclock2 = mclock();
  cout<<"Time of CPU-GPU copy: "<<diffclock(aclock1,aclock2)<<"s"<<endl;  

  //
  // KERNEL LAUNCH:
  //
  int block_size = 4;
  int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);
  
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaEventRecord(start);  
  square_array <<< n_blocks, block_size >>> (a_d, N);
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  cout<<"Kernel execution time: "<< milliseconds/1000.0 <<"s"<<endl;    

  // Retrieve result from device and store it in host array
  double abclock1, abclock2;
  cudaDeviceSynchronize();
  abclock1 = mclock();
  cudaMemcpy( a_h, a_d, sizeof(double)*N, cudaMemcpyDeviceToHost );
  cudaDeviceSynchronize();
  abclock2 = mclock();
  cout<<"Time of GPU-CPU copy: "<<diffclock(abclock1,abclock2)<<"s"<<endl;  

  // Print results
  //for (int i=0; i<10; i++) printf("%d %lf\n", i, a_h[i]);

  // Cleanup
  //free(a_h); 
  cudaFreeHost(a_h);
  
  cudaFree(a_d);

  cout<<"end of program"<<endl;
  cout<<endl;
  
  return 0;

}

int CUDA_test_cuFFT( int N ){
  using namespace std;
  
  int Nfft = 10;

  cufftHandle plan;
  cufftDoubleComplex *data;
  int n[NRANK] = {N, N};

  cout<<"N = "<<N<<endl;
  cout<<"Forward cuFFT..."<<endl;

  cudaMalloc((void**)&data, sizeof(cufftDoubleComplex)*N*N );
  if (cudaGetLastError() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to allocate\n");
    return 1;
  }
  
//  for( long int i = 0; i < N*N; i++ ){
//    data[i] = make_cuDoubleComplex(1, 1);
//  }

  /* Create a 2D FFT plan. */
  if (cufftPlanMany(&plan, NRANK, n, NULL, 1, 0, NULL, 1, 0, CUFFT_Z2Z,BATCH) != CUFFT_SUCCESS){
    fprintf(stderr, "CUFFT Error: Unable to create plan\n");
    return 1;	
  }


  double abclock1, abclock2;
  cudaDeviceSynchronize();
  abclock1 = mclock();
  
  for( int i = 0; i < Nfft; i++){
    if (cufftExecZ2Z(plan, data, data, CUFFT_FORWARD) != CUFFT_SUCCESS){
      fprintf(stderr, "CUFFT Error: Unable to execute plan\n");
      return 1;
    }
  }

  if (cudaDeviceSynchronize() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to synchronize\n");
    return 1;
  }

  abclock2 = mclock();
  cout<<"Time of 2d cuFFT computation: "<<diffclock(abclock1,abclock2)/1.0/Nfft<<"s"<<endl;  

  cout<<endl;
        

  cufftDestroy(plan);
  cudaFree(data);

  return 0;
}


int test_CUDA_NonInterval( int N1, int N2, int dN ){
  using namespace std;
  
  if( N1 == N2) dN = 1;
  for( int Ni = N1; Ni <= N2; Ni = Ni + dN ){
    //CUDA_dataCopy_testTime( Ni );
    CUDA_test_cuFFT( Ni );
  }
  return 0;
}
              

int main(int argc, char **argv){

  using namespace std;
  
  int np = 1;
  int myrank = 0;
        
  int N=2*1024;
  int N1 = N;
  int N2 = N;
  int dN = 0;
                        
  if(argc > 1){
    N = atoi(argv[1]);
  }else{
    N = 100;
  }
  N1 = N;
  N2 = N;
  dN = 0;

  if(argc == 3){
    N1 = atoi(argv[1]);
    N2 = atoi(argv[2]);
    N = N1;
    dN = N2 - N1;
  }
                                                                      
  if(argc == 4){
    N1 = atoi(argv[1]);
    N2 = atoi(argv[2]);
    dN = atoi(argv[3]);
    N = N1;
  }
                              
  if (myrank == 0) cout<<endl<<"N1="<<N1<<" N2="<<N2<<" dN="<<dN<<endl;
  if (myrank == 0) cout << endl;
                                                                                              
  double aclock1,aclock2;
  aclock1 = mclock();

  test_CUDA_NonInterval( N1, N2, dN );
  
  aclock2 = mclock();
  if(myrank==0) cout<<"Total time: "<<diffclock(aclock1,aclock2)<<"s"<<endl;
      
  return 0;

}