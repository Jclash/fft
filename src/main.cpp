// main.cpp : Defines the entry point for the console application.
// v1.0.2

//#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <fstream>
#include <iostream>
//#include <complex>
#include <string>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <sstream>
#include "time.h"

#include <sys/time.h>
#include <sys/resource.h>

//#include <memory>
//#include <utility>

//#include "output.h"

#include "fftw3-mpi.h"
//#include "fftw_mpi.h"

#include "mpi.h"
//#include "fftw3.h"
//#include "MtoBMP.h"

//#include "mkl_service.h"
//#include "mkl_cdft.h"

// BOOST library
//#include <boost/lexical_cast.hpp>

#define PREC DFTI_DOUBLE

//#include "mkl_dfti.h"

#include "fftw3.h"


#include "time.h"
#include "math.h"
#include "stdlib.h"
#include <iostream>
//#include <ctime>

typedef double double_complex[2];

double diffclock(double clock1,double clock2)
{
  //return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
  //return ((clock2-clock1)*1000.0)/(double)CLOCKS_PER_SEC;
  return clock2-clock1;
}

double mclock(){
  timeval tim;
  gettimeofday(&tim, NULL);
  return tim.tv_sec+(tim.tv_usec/1000000.0);
}

double getcputime(void){
	struct timeval tim;
    struct rusage ru;

    getrusage(RUSAGE_SELF, &ru);
    tim=ru.ru_utime;
    double t=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
    tim=ru.ru_stime;
    t+=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;

    getrusage(RUSAGE_CHILDREN, &ru);
    tim=ru.ru_utime;
    t += (double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
    tim=ru.ru_stime;
    t+=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
    return t;
}


/*
int test_MKL_1d(int N=2*1024*1024){
  using namespace std;

  double_complex *x,*x_in;
  int Nmax=10;

  DFTI_DESCRIPTOR_HANDLE my_desc1_handle;
  DFTI_DESCRIPTOR_HANDLE my_desc2_handle;
  MKL_LONG status;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  x = (double_complex*)malloc(sizeof(double_complex)*N);
  x_in = (double_complex*)malloc(sizeof(double_complex)*N);

  for(int i=0;i<N;i++){
    x_in[i][0]=0.0;
    x_in[i][1]=i;
  }

  for(int i=0;i<N;i++){
    x[i][0]=x_in[i][0];
    x[i][1]=x_in[i][1];
  }

  // Forward
  status = DftiCreateDescriptor( &my_desc1_handle, DFTI_DOUBLE, DFTI_COMPLEX, 1, N);
  status = DftiCommitDescriptor( my_desc1_handle );

  fclock1 = mclock();
  status = DftiComputeForward( my_desc1_handle, x);
  fclock2 = mclock();

  // Backward
  status = DftiCreateDescriptor( &my_desc2_handle, DFTI_DOUBLE, DFTI_COMPLEX, 1, N);
  status = DftiCommitDescriptor( my_desc2_handle );

  bclock1 = mclock();
  status = DftiComputeBackward( my_desc2_handle, x);
  bclock2 = mclock();

  for(int i=0;i<N;i++){
    x[i][0]/=N;
    x[i][1]/=N;
  }

  // Out
  cout<<"N = "<<N<<endl;

  cout<<"Input array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
    cout<<"x["<<i<<",0]="<<x_in[i][0];
    cout<<"; x["<<i<<",1]="<<x_in[i][1]<<endl;
  }
  if(Nmax<N) cout<<"..."<<endl;
  cout<<endl;

  cout<<"Output array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
    cout<<"x["<<i<<",0]="<<x[i][0];
    cout<<"; x["<<i<<",1]="<<x[i][1]<<endl;
  }
  if(Nmax<N) cout<<"..."<<endl;
  cout<<endl<<endl;

  cout<<"Memmory allocated for one array: "<<(sizeof(double_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  cout<<"Forward DFT time: "<<diffclock(fclock1,fclock2)<<"ms"<<endl;
  cout<<"Backward DFT time: "<<diffclock(bclock1,bclock2)<<"ms"<<endl;

  double err=0.0,terr;
  for(int i=0; i<N;i++){
    terr = fabs(sqrt(x[i][0]*x[i][0]+x[i][1]*x[i][1])-sqrt(x_in[i][0]*x_in[i][0]+x_in[i][1]*x_in[i][1]));
    if(terr>err) err = terr;
  }
  cout<<"Error = "<<err<<endl;

  //cin>>x[0][0];

  status = DftiFreeDescriptor(&my_desc1_handle);
  status = DftiFreeDescriptor(&my_desc2_handle);
  free(x_in);
  free(x);

  return 0;
}
*/

int test_FFTW_1d(int N=2*1024*1024){
  using namespace std;

  fftw_complex *x,*x_in;
  int Nmax=10;

  fftw_plan forward_plan;
  fftw_plan backward_plan;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  cout<<endl<<"FFTW test "<<endl;
  cout<<"N = "<<N<<endl<<endl;

  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);

  forward_plan = fftw_plan_dft_1d(N,x,x,FFTW_FORWARD,FFTW_MEASURE);
  backward_plan = fftw_plan_dft_1d(N,x,x,FFTW_BACKWARD,FFTW_MEASURE);

  srand ( time(NULL) );
  for(int i=0;i<N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }

  for(int i=0;i<N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }


  // Forward

  cout<<"Forward DFT...";
  fclock1 = mclock();
  fftw_execute(forward_plan);
  fclock2 = mclock();
  cout<<endl;

  // Backward

  cout<<"Backward DFT...";
  bclock1 = mclock();
  fftw_execute(backward_plan);
  bclock2 = mclock();
  cout<<endl;

  for(int i=0;i<N;i++){
    x[i][0]/=N;
    x[i][1]/=N;
  }

  // Out

  cout<<endl;
  cout<<"Input array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
    cout<<"x["<<i<<",0]="<<x_in[i][0];
    cout<<"; x["<<i<<",1]="<<x_in[i][1]<<endl;
  }
  if(Nmax<N) cout<<"..."<<endl;
  cout<<endl;

  cout<<"Output array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
    cout<<"x["<<i<<",0]="<<x[i][0];
    cout<<"; x["<<i<<",1]="<<x[i][1]<<endl;
  }
  if(Nmax<N) cout<<"..."<<endl;
  cout<<endl<<endl;

  cout<<"Memmory allocated for one array: "<<(sizeof(double_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  cout<<"Forward DFT time: "<<diffclock(fclock1,fclock2)<<"ms"<<endl;
  cout<<"Backward DFT time: "<<diffclock(bclock1,bclock2)<<"ms"<<endl;

  double err=0.0,terr;
  for(int i=0; i<N;i++){
    terr = fabs(sqrt(x[i][0]*x[i][0]+x[i][1]*x[i][1])-sqrt(x_in[i][0]*x_in[i][0]+x_in[i][1]*x_in[i][1]));
    if(terr>err) err = terr;
  }
  cout<<"Error = "<<err<<endl;

  double add,mult,fma;
  fftw_flops(forward_plan,&add,&mult,&fma);
  cout<<"FLOPs for forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  fftw_flops(backward_plan,&add,&mult,&fma);
  cout<<"FLOPs for backward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  //cin>>x[0][0];

  fftw_destroy_plan(forward_plan);
  fftw_destroy_plan(backward_plan);
  fftw_cleanup();
  fftw_free(x_in);
  fftw_free(x);

  return 0;
}

int test_FFTW_1d_MPI(int N=2*1024*1024){
  using namespace std;

  int myrank, Nfft = 10;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x,*x_in;
  int Nmax=10;

  fftw_plan forward_plan;
  fftw_plan backward_plan;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  if(myrank==0) cout<<endl<<"FFTW test "<<endl;
  if(myrank==0) cout<<"N = "<<N<<endl<<endl;

  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);

  forward_plan = fftw_plan_dft_1d(N,x,x,FFTW_FORWARD,FFTW_ESTIMATE);
  backward_plan = fftw_plan_dft_1d(N,x,x,FFTW_BACKWARD,FFTW_ESTIMATE);

  srand ( time(NULL) );
  for(int i=0;i<N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }

  for(int i=0;i<N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }


  // Forward

  if(myrank==0) cout<<"Forward DFT...";
  MPI_Barrier(MPI_COMM_WORLD);
  fclock1 = mclock();
  for(int i=0;i<Nfft;i++) fftw_execute(forward_plan);
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Backward

  if(myrank==0) cout<<"Backward DFT...";
  MPI_Barrier(MPI_COMM_WORLD);
  bclock1 = mclock();
  for(int i=0;i<Nfft;i++)  fftw_execute(backward_plan);
  bclock2 = mclock();
  if(myrank==0) cout<<endl;

  for(int i=0;i<N;i++){
    x[i][0]/=N;
    x[i][1]/=N;
  }

  // Out

  if(myrank==0) cout<<endl;
  if(myrank==0) cout<<"Input array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x_in[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x_in[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl;

  if(myrank==0) cout<<"Output array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl<<endl;

  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Forward DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"ms"<<endl;
  if(myrank==0) cout<<"One Backward DFT time: "<<diffclock(bclock1,bclock2)/Nfft<<"ms"<<endl;

  double err=0.0,terr;
  for(int i=0; i<N;i++){
    terr = fabs(sqrt(x[i][0]*x[i][0]+x[i][1]*x[i][1])-sqrt(x_in[i][0]*x_in[i][0]+x_in[i][1]*x_in[i][1]));
    if(terr>err) err = terr;
  }
  if(myrank==0) cout<<"Error = "<<err<<endl;

  double add,mult,fma;
  fftw_flops(forward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  fftw_flops(backward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one backward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  //cin>>x[0][0];

  fftw_destroy_plan(forward_plan);
  fftw_destroy_plan(backward_plan);
  fftw_cleanup();
  fftw_free(x_in);
  fftw_free(x);

  return 0;
}

int test_mult_2d_MPI(int NN=1024 ){
  using namespace std;

  int myrank, Nfft = 100;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x, *x_in;
  int Nmax = 10;
  int N =  NN;

  fftw_plan forward_plan;
  fftw_plan backward_plan;


 double fclock1, fclock2;
 double bclock1, bclock2;
 double mclock1, mclock2;

  if(myrank==0) cout<<endl<<"FFTW test "<<endl;
  if(myrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl<<endl;

  // Init

  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);

  srand ( time(NULL) );
  for(int i=0;i<N*N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }

  for(int i=0;i<N*N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }

  // Mult

  if(myrank==0) cout<<"Multiplication...";
  MPI_Barrier(MPI_COMM_WORLD);
  mclock1 = mclock();
  for(int i=0;i<Nfft;i++){
	  for(int j =0; j < N*N; j++){
		  x_in[j][0] = x_in[j][0]*x[j][0];
		  x_in[j][1] = x_in[j][1]*x[j][1];
	  }
  };
  mclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Out

  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One multiplication time: "<<diffclock(mclock1,mclock2)/Nfft<<"s"<<endl;

  // End

  fftw_cleanup();
  fftw_free(x_in);
  fftw_free(x);

  return 0;
}

int test_fFFTW_bFFTW_mult_2d_MPI(int NN=1024 ){
  using namespace std;

  int myrank, Nfft = 100;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x, *x_in;
  int Nmax = 10;
  int N =  NN;

  fftw_plan forward_plan;
  fftw_plan backward_plan;


 double fclock1, fclock2;
 double bclock1, bclock2;
 double mclock1, mclock2;

  if(myrank==0) cout<<endl<<"FFTW test "<<endl;
  if(myrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl<<endl;

  // Init

  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);

  //forward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_FORWARD,FFTW_MEASURE);
  //backward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_BACKWARD,FFTW_MEASURE);

  forward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_FORWARD,FFTW_ESTIMATE);
  backward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_BACKWARD,FFTW_ESTIMATE);

  srand ( time(NULL) );
  for(int i=0;i<N*N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }

  for(int i=0;i<N*N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }

  // Forward

  if(myrank==0) cout<<"Forward DFT...";
  MPI_Barrier(MPI_COMM_WORLD);
  fclock1 = mclock();
  for(int i=0;i<Nfft;i++) fftw_execute(forward_plan);
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Backward

  if(myrank==0) cout<<"Backward DFT...";
  MPI_Barrier(MPI_COMM_WORLD);
  bclock1 = mclock();
  for(int i=0;i<Nfft;i++)  fftw_execute(backward_plan);
  bclock2 = mclock();
  if(myrank==0) cout<<endl;

  for(int j=0;j<Nfft;j++){
    for(int i=0;i<N*N;i++){
      x[i][0]/=N*N;
      x[i][1]/=N*N;
    }
  }

  double err=0.0,terr;
  for(int i=0; i<N*N;i++){
    terr = fabs(sqrt(x[i][0]*x[i][0]+x[i][1]*x[i][1])-sqrt(x_in[i][0]*x_in[i][0]+x_in[i][1]*x_in[i][1]));
    if(terr>err) err = terr;
  }

  // Mult

  if(myrank==0) cout<<"Multiplication...";
  MPI_Barrier(MPI_COMM_WORLD);
  mclock1 = mclock();
  for(int i=0;i<Nfft;i++){
	  for(int j =0; j < N*N; j++){
		  x_in[j][0] = x_in[j][0]*x[j][0];
		  x_in[j][1] = x_in[j][1]*x[j][1];
	  }
  };
  mclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Out

  if(myrank==0) cout<<endl;
  if(myrank==0) cout<<"Input array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x_in[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x_in[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl;

  if(myrank==0) cout<<"Output array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl<<endl;

  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;
  if(myrank==0) cout<<"One multiplication time: "<<diffclock(mclock1,mclock2)/Nfft<<"s"<<endl;
  if(myrank==0) cout<<"One Backward 2d DFT time: "<<diffclock(bclock1,bclock2)/Nfft<<"s"<<endl;

  if(myrank==0) cout<<"Error = "<<err<<endl;

  double add,mult,fma;
  fftw_flops(forward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  fftw_flops(backward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one backward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  //cin>>x[0][0];

  fftw_destroy_plan(forward_plan);
  fftw_destroy_plan(backward_plan);
  fftw_cleanup();
  fftw_free(x_in);
  fftw_free(x);

  return 0;
}




int test_fFFTW_2d_MPI(int NN, int Nfft ){
  using namespace std;

  int myrank;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x, *x_in;
  int Nmax = 10;
  int N =  NN;

  fftw_plan forward_plan;
  fftw_plan backward_plan;


 double fclock1, fclock2;
 double bclock1, bclock2;
 double mclock1, mclock2;

  if(myrank==0) cout<<endl<<"FFTW test "<<endl;
  if(myrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl;

  // Init

//  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);
  x = fftw_alloc_complex(N*N);
//  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);

  forward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_FORWARD,FFTW_MEASURE);

  //forward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_FORWARD,FFTW_ESTIMATE);

  for(int i=0;i<N*N;i++){
    x[i][0] = i;
    x[i][1] = i;
  }

  // Forward

  if(myrank==0) cout<<"Forward DFT...";
  MPI_Barrier(MPI_COMM_WORLD);
  fclock1 = mclock();
  MPI_Barrier(MPI_COMM_WORLD);
  for(int i=0;i<Nfft;i++) fftw_execute(forward_plan);
  MPI_Barrier(MPI_COMM_WORLD);
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Out

  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(forward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  fftw_destroy_plan(forward_plan);
  fftw_cleanup();
  //fftw_free(x_in);
  fftw_free(x);

  return 0;
}



int test_fFFTW_2d_distr_MPI_clean(int N, int Nfft)
{
  using namespace std;

  int myrank;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);


  const ptrdiff_t N0 = N, N1 = N;
  fftw_plan plan;
  fftw_complex *data;
  ptrdiff_t alloc_local, local_n0, local_0_start, i, j;

  if(myrank==0) cout<<"FFT test (fftw)"<<endl;
  if(myrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl;

//    MPI_Init(&argc, &argv);
  fftw_mpi_init();

  /* get local data size and allocate */
  alloc_local = fftw_mpi_local_size_2d(N0, N1, MPI_COMM_WORLD,&local_n0, &local_0_start);
  data = fftw_alloc_complex(alloc_local);

  if(myrank==0) cout<<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"Memory allocated on the core 0: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;


  /* create plan for in-place forward DFT */
  plan = fftw_mpi_plan_dft_2d(N0, N1, data, data, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE);

  /* initialize data to some function my_function(x,y) */
  for (i = 0; i < local_n0; ++i) for (j = 0; j < N1; ++j){
    data[i*N1 + j][0] = 1.1;//(local_0_start + i) - (j);
    data[i*N1 + j][1] = 3.1;
  }

    /* compute transforms, in-place, as many times as desired */
    //fftw_execute(plan);


  double fclock1, fclock2;
  if(myrank==0) cout<<"Forward DFT...";
  fclock1 = mclock();
  for(int i=0;i<Nfft;i++) fftw_execute(plan);
  fclock2 = mclock();

  if(myrank==0) cout<<endl;
  if(myrank==0) cout<<"One Distributed Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  if(myrank==0) cout<<endl;

  fftw_destroy_plan(plan);
  fftw_free(data);

//    MPI_Finalize();
}

int test_fFFTW_2d_distr_MPI(int NN=1024 ){
  using namespace std;

  fftw_mpi_init();

  int myrank, Nfft = 1;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x, *x_in;
  int Nmax = 10;
  int N =  NN;

  fftw_plan forward_plan;

  double fclock1, fclock2;
  double bclock1, bclock2;
  double mclock1, mclock2;

  const ptrdiff_t T_N0 = NN, T_N1 = NN;
  ptrdiff_t T_alloc_local, T_local_n0, T_local_0_start, gi, gj;

  if(myrank==0) cout<<endl<<"FFTW test "<<endl;
  if(myrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl<<endl;

  // Init

  T_alloc_local = fftw_mpi_local_size_2d(T_N0, T_N1, MPI_COMM_WORLD, &T_local_n0, &T_local_0_start);
  x = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * T_alloc_local);

  if(myrank==0) cout<<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"Memory allocated on the core 0: "<<(sizeof(fftw_complex)*T_alloc_local)/1024.0/1024.0<<" Mb"<<endl;

  //forward_plan = fftw_mpi_plan_dft_2d(T_N0, T_N1, x, x, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE);
  forward_plan = fftw_mpi_plan_dft_2d(T_N0, T_N1, x, x, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);
  cout<<"Plan constructed"<<endl;

  //forward_plan = fftw_plan_dft_2d(N,N,x,x,FFTW_FORWARD,FFTW_ESTIMATE);

  for(int i=0;i<T_alloc_local;i++){
    x[i][0] = i;
    x[i][1] = i;
  }
  cout<<"Local arrays filled"<<endl;

  // Forward

  if(myrank==0) cout<<"Forward DFT...";
  fftw_execute(forward_plan);
  cout<<"Plan executed"<<endl;


  //MPI_Barrier(MPI_COMM_WORLD);
  //fclock1 = mclock();
  //for(int i=0;i<Nfft;i++) fftw_execute(forward_plan);
  //fclock2 = mclock();
  //if(myrank==0) cout<<endl;

  // Out

//  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Distributed Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(forward_plan,&add,&mult,&fma);
  if(myrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  fftw_destroy_plan(forward_plan);
  fftw_cleanup();
  fftw_free(x_in);
  fftw_free(x);

  return 0;
}

int fwrite( fftw_complex* x, int N, const char* fname ){
  FILE* f;
  f = fopen( fname,"w+b");
  fwrite(x,sizeof(fftw_complex),N,f);
  fflush(f);
  fclose(f);

  return 0;
}

int fread( fftw_complex* x, int N, const char* fname ){
  FILE* f;
  f = fopen( fname,"r+b");
  fread(x,sizeof(fftw_complex),N,f);
  fclose(f);

  return 0;
}

int test_fRW(int N=2*1024*1024){
  using namespace std;

  int myrank, Ncount = 1;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  if(myrank==0){

  fftw_complex *x,*x_in;
  int Nmax=10;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  if(myrank==0) cout<<endl<<"HDD test "<<endl;
  if(myrank==0) cout<<"N = "<<N<<endl<<endl;
  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;

  // File init
  //char* fname = "media/VirtualBox/file.bin";
  char* fname = "file.bin";
  FILE* f;
  //FILE* f = fopen("/media/VirtualBox/file.bin","w+b");
  //FILE* f = fopen("file.bin","w+b");

  // Arrays init
  x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);

  srand ( time(NULL) );
  for(int i=0;i<N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }
/*
  for(int i=0;i<N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }
*/

  // Write

  if(myrank==0) cout<<"Write...";
  fclock1 = mclock();
    fwrite( x_in, N, fname );
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Read
  if(myrank==0) cout<<"Read...";
  bclock1 = mclock();
    f = fopen( fname,"r+b");
    //fread(x,sizeof(fftw_complex),N,f);
    fclose(f);
  bclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Out

  if(myrank==0) cout<<endl;
  if(myrank==0) cout<<"Input array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x_in[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x_in[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl;

  if(myrank==0) cout<<"Output array:"<<endl;
  for(int i=0; (i<N)&&(i<Nmax);i++){
	  if(myrank==0) cout<<"x["<<i<<",0]="<<x[i][0];
	  if(myrank==0) cout<<"; x["<<i<<",1]="<<x[i][1]<<endl;
  }
  if(Nmax<N){ if(myrank==0) cout<<"..."<<endl;};
  if(myrank==0) cout<<endl<<endl;

  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Write time: "<<diffclock(fclock1,fclock2)/Ncount<<"ms"<<endl;
  if(myrank==0) cout<<"One Read time: "<<diffclock(bclock1,bclock2)/Ncount<<"ms"<<endl;
  if(myrank==0) cout<<"Overall time: "<<diffclock(fclock1,bclock2)<<"ms"<<endl;

  double err=0.0,terr;
  for(int i=0; i<N;i++){
    terr = fabs(sqrt(x[i][0]*x[i][0]+x[i][1]*x[i][1])-sqrt(x_in[i][0]*x_in[i][0]+x_in[i][1]*x_in[i][1]));
    if(terr>err) err = terr;
  }
  if(myrank==0) cout<<"Error = "<<err<<endl;



  fftw_free(x_in);
  fftw_free(x);

  }

  MPI_Barrier(MPI_COMM_WORLD);

  return 0;
}

int test_1000writes(int N=2*1024*1024, int Ncount = 10){
  using namespace std;

  int myrank;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  if(myrank==0){

  fftw_complex *x,*x_in;
  int Nmax=10;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  if(myrank==0) cout<<endl<<"HDD test "<<endl;
  if(myrank==0) cout<<"N = "<<N<<endl<<endl;
  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;

  // File init
  //char* fname = "/media/VirtualBox/file.bin";
  char* fname;
  char folder[256] = "/media/VirtualXP/";
  char fnum[10];
  std::string folder_str = folder;
  std::string fname_str;
  std::string fnume_str = fnum;
  FILE* f;

  cout<<endl<<"fname = "<<fname_str;

  // Array init
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);

  srand ( time(NULL) );
  for(int i=0;i<N;i++){
    x_in[i][0] = rand() % N;
    x_in[i][1] = rand() % N;
  }
/*
  for(int i=0;i<N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }
*/

  // Write

  if(myrank==0) cout<<"Write...";
  fclock1 = mclock();
    for(int i=0;i<Ncount;i++){
      sprintf( fnum, "%d", (int) i );
      fname_str = folder_str + "f"+fnum+".bin";
      fwrite( x_in, N, fname_str.c_str() );
    };
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Read
  /*
  if(myrank==0) cout<<"Read...";
  bclock1 = mclock();
    f = fopen( fname,"r+b");
    fread(x,sizeof(fftw_complex),N,f);
    fclose(f);
  bclock2 = mclock();
  if(myrank==0) cout<<endl;
  */

  // Out


  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Write time: "<<diffclock(fclock1,fclock2)/(double)Ncount<<"s"<<endl;
  if(myrank==0) cout<<"Write speed: "<<((sizeof(fftw_complex)*N)/1024.0/1024.0)/(diffclock(fclock1,fclock2)/Ncount)<<"Mb/s"<<endl;
  if(myrank==0) cout<<"Overall time: "<<diffclock(fclock1,fclock2)<<"s"<<endl;


  fftw_free(x_in);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return 0;
}

int test_1000reads(int N=2*1024*1024, int Ncount = 10){
  using namespace std;

  int myrank;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  if(myrank==0){

  fftw_complex *x,*x_in;
  int Nmax=10;

  time_t ftime1,ftime2;
  time_t btime1,btime2;

 double fclock1,fclock2;
 double bclock1,bclock2;

  if(myrank==0) cout<<endl<<"HDD test "<<endl;
  if(myrank==0) cout<<"N = "<<N<<endl<<endl;
  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;

  // File init
  //char* fname = "/media/VirtualBox/file.bin";
  char* fname;
  char folder[256] = "/media/VirtualXP/";
  char fnum[10];
  std::string folder_str = folder;
  std::string fname_str;
  std::string fnume_str = fnum;
  FILE* f;

  //cout<<endl<<"fname = "<<fname_str;

  // Array init
  x_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N);

  srand ( time(NULL) );
  for(int i=0;i<N;i++){
    x_in[i][0] = 0.0;
    x_in[i][1] = 0.0;
  }
/*
  for(int i=0;i<N;i++){
    (*(x+i))[0] = x_in[i][0];
    (*(x+i))[1] = x_in[i][1];
  }
*/

  // Write

  double res = 0.0;
  if(myrank==0) cout<<"Read...";
  fclock1 = mclock();
    for(int i=0;i<Ncount;i++){
      sprintf( fnum, "%d", (int) i );
      fname_str = folder_str + "f"+fnum+".bin";
      fread( x_in, N, fname_str.c_str() );
      //fwrite( x_in, N, fname_str.c_str() );
      //res = 0.0;
      //for(int j = 0; j<N; j++) res += x_in[j][0]+x_in[j][1];
      //cout<<endl<<"res = "<<res;
    };
  fclock2 = mclock();
  if(myrank==0) cout<<endl;

  // Read
  /*
  if(myrank==0) cout<<"Read...";
  bclock1 = mclock();
    f = fopen( fname,"r+b");
    fread(x,sizeof(fftw_complex),N,f);
    fclose(f);
  bclock2 = mclock();
  if(myrank==0) cout<<endl;
  */

  // Out


  if(myrank==0) cout<<"Memmory allocated for one array: "<<(sizeof(fftw_complex)*N)/1024.0/1024.0<<" Mb"<<endl;
  if(myrank==0) cout<<"One Read time: "<<diffclock(fclock1,fclock2)/Ncount<<"s"<<endl;
  if(myrank==0) cout<<"Read speed: "<<((sizeof(fftw_complex)*N)/1024.0/1024.0)/(diffclock(fclock1,fclock2)/Ncount)<<"Mb/s"<<endl;
  if(myrank==0) cout<<"Overall time: "<<diffclock(fclock1,fclock2)<<"s"<<endl;


  fftw_free(x_in);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return 0;
}


double f( double x ){ return x ;}

double g( double x ){ return 0.0;}

double h( double k, double x, double* re, double* im ){
  *re = f(x)*cos(k*g(x));
  *im = f(x)*sin(k*g(x));
}

double int_darbu_1d( double k, int N, double* re, double* im ){
using namespace std;

  double w = 1.0/N;
  double cur_re, cur_im;

  *re = 0.0;
  *im = 0.0;

  for( int i = 0; i < N; i++){
    h( k, w*i, &cur_re, &cur_im );
    *re += w*(cur_re);
    *im += w*(cur_im);

  }

  cout<<" re="<< *re <<" im="<< *im <<endl;

  return 0;
}


int test_fFFTWs_2d_distr_MPI_Wgroups(int N, int Nc, double M0, int Nfft)
{
  using namespace std;

  int oldrank;
  int oldnp;

  MPI_Comm_rank(MPI_COMM_WORLD,&oldrank);
  MPI_Comm_size(MPI_COMM_WORLD,&oldnp);

  MPI_Barrier(MPI_COMM_WORLD);

  int W = floor(M0/(N*1.0*N));
  int Nc_big = floor(Nc*1.0/W) + 1;
  int Nc_small = floor(Nc*1.0/W);
  int C1 = (Nc - Nc_small*W)*Nc_big;
  int C0 = (W - (Nc - Nc_small*W))*Nc_small;

  int color, newrank;

  if( oldrank < C1){
    color = oldrank/Nc_big;
  }else{
    color = (oldrank - C1)/Nc_small + C1;
  }

  if( oldrank < C1){
    newrank = oldrank - color*Nc_big;
  }else{
    newrank = oldrank - C1 - Nc_small*(color - (Nc - Nc_small*W));
  }

  if(oldrank == 0) cout<<"M0="<<M0<<" Nn="<<1<<" Nc="<<Nc<<" W="<<W<<endl;

  MPI_Comm COMMi;

  MPI_Comm_split( MPI_COMM_WORLD, color, newrank, &COMMi );

  int myrank, np;
  MPI_Comm_rank(COMMi,&myrank);
  MPI_Comm_size(COMMi,&np);

  ofstream myfile;
  stringstream sstream;
  sstream << oldrank;
  string fnamestr = "proc" + sstream.str() + ".txt";
  myfile.open ( fnamestr.c_str() );
  myfile << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //myfile.close();

  //cout << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //cout<<" np="<< np;

  //MPI_Barrier(MPI_COMM_WORLD);
  //return 0;

  const ptrdiff_t N0 = N, N1 = N;
  fftw_plan plan;
  fftw_complex *data;
  ptrdiff_t alloc_local, local_n0, local_0_start, i, j;

  if(oldrank==0) cout<<"FFT test (fftw)"<<endl;
  if(oldrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl;
  myfile <<"FFT test (fftw)"<<endl;
  myfile << "N x N = "<<N<<" x "<<N<<endl;

//    MPI_Init(&argc, &argv);
  fftw_mpi_init();

  /* get local data size and allocate */
  alloc_local = fftw_mpi_local_size_2d(N0, N1, COMMi, &local_n0, &local_0_start);
  data = fftw_alloc_complex(alloc_local);

  if(oldrank==0) cout<<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(oldrank==0) cout<<"Memory allocated on the core 0: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Memory allocated on this core: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;

  /* create plan for in-place forward DFT */
  plan = fftw_mpi_plan_dft_2d( N0, N1, data, data, COMMi, FFTW_FORWARD, FFTW_ESTIMATE );

  /* initialize data to some function my_function(x,y) */
  for (i = 0; i < local_n0; ++i) for (j = 0; j < N1; ++j){
    data[i*N1 + j][0] = 1.1;//(local_0_start + i) - (j);
    data[i*N1 + j][1] = 3.1;
  }

    /* compute transforms, in-place, as many times as desired */
    //fftw_execute(plan);


  double fclock1, fclock2;
  double flastclock1, flastclock2;
  if( oldrank == 0 ) cout<<"Forward DFT...";
  myfile<<"Forward DFT...";
  fclock1 = mclock();
  flastclock1 = mclock();
  for( int ii = 0; ii < Nfft; ii++ ){
    fftw_execute( plan );
  }
  fclock2 = mclock();
  MPI_Barrier( MPI_COMM_WORLD );
  flastclock2 = mclock();

  if(oldrank==0) cout<<endl;
  if(oldrank==0) cout<<"One Distributed Forward 2d DFT time: "<<diffclock(flastclock1,flastclock2)/Nfft<<"s"<<endl;
  myfile<<"One Distributed Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(plan,&add,&mult,&fma);
  if(oldrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  if(oldrank==0) cout<<endl;
  myfile <<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  myfile.close();

  fftw_destroy_plan( plan );
  fftw_cleanup();
  fftw_free( data );

  MPI_Barrier( MPI_COMM_WORLD );


//    MPI_Finalize();
}



int test_fFFTWs_2d_distr_MPI_Wgroups_10P(int N, int Nc, double M0, int Nfft)
{
  using namespace std;

  int oldrank;
  int oldnp;

  MPI_Comm_rank(MPI_COMM_WORLD,&oldrank);
  MPI_Comm_size(MPI_COMM_WORLD,&oldnp);

  MPI_Barrier(MPI_COMM_WORLD);

  int W = floor(M0/(N*1.0*N));
  int Nc_big = floor(Nc*1.0/W) + 1;
  int Nc_small = floor(Nc*1.0/W);
  int C1 = (Nc - Nc_small*W)*Nc_big;
  int C0 = (W - (Nc - Nc_small*W))*Nc_small;

  int color, newrank;

  if(oldrank == 0) cout<<"Special procedure for 10P is runned"<<endl;

  if( oldrank < C1){
    color = oldrank/Nc_big;
  }else{
    color = (oldrank - C1)/Nc_small + C1;
  }

  if( oldrank < C1){
    newrank = oldrank - color*Nc_big;
  }else{
    newrank = oldrank - C1 - Nc_small*(color - (Nc - Nc_small*W));
  }

  if(oldrank == 0) cout<<"M0="<<M0<<" Nn="<<1<<" Nc="<<Nc<<" W="<<W<<endl;

  MPI_Comm COMMi;

  //MPI_Comm_split( MPI_COMM_WORLD, color, newrank, &COMMi );
  //MPI_Comm_split( MPI_COMM_WORLD, 0, oldrank, &COMMi );

  int myrank, np;
  MPI_Comm_rank(COMMi,&myrank);
  MPI_Comm_size(COMMi,&np);

  ofstream myfile;
  stringstream sstream;
  sstream << oldrank;
  string fnamestr = "proc" + sstream.str() + ".txt";
  myfile.open ( fnamestr.c_str() );
  myfile << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //myfile.close();

  //cout << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //cout<<" np="<< np;

  //MPI_Barrier(MPI_COMM_WORLD);
  //return 0;

  const ptrdiff_t N0 = N, N1 = N;
  fftw_plan plan;
  fftw_complex *data;
  ptrdiff_t alloc_local, local_n0, local_0_start, i, j;

  if(oldrank==0) cout<<"FFT test (fftw)"<<endl;
  if(oldrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl;
  myfile <<"FFT test (fftw)"<<endl;
  myfile << "N x N = "<<N<<" x "<<N<<endl;

//    MPI_Init(&argc, &argv);
  fftw_mpi_init();

  /* get local data size and allocate */
  alloc_local = fftw_mpi_local_size_2d(N0, N1, MPI_COMM_WORLD, &local_n0, &local_0_start);
  data = fftw_alloc_complex(alloc_local);

  if(oldrank==0) cout<<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(oldrank==0) cout<<"Memory allocated on the core 0: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Memory allocated on this core: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;

  /* create plan for in-place forward DFT */
  plan = fftw_mpi_plan_dft_2d( N0, N1, data, data, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE );

  /* initialize data to some function my_function(x,y) */
  for (i = 0; i < local_n0; i++){
    for (j = 0; j < N1; j++){
      data[i*N1 + j][0] = 1.1;//(local_0_start + i) - (j);
      data[i*N1 + j][1] = 3.1;
    }
  }

    /* compute transforms, in-place, as many times as desired */
    //fftw_execute(plan);


  double fclock1, fclock2;
  double flastclock1, flastclock2;
  if( oldrank == 0 ) cout<<"Forward DFT...";
  myfile<<"Forward DFT...";
  fclock1 = mclock();
  flastclock1 = mclock();
  for( int ii = 0; ii < Nfft; ii++ ){
    fftw_execute( plan );
  }
  fclock2 = mclock();
  MPI_Barrier( MPI_COMM_WORLD );
  flastclock2 = mclock();

  if(oldrank==0) cout<<endl;
  if(oldrank==0) cout<<"One Distributed Forward 2d DFT time: "<<diffclock(flastclock1,flastclock2)/Nfft<<"s"<<endl;
  myfile<<"One Distributed Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(plan,&add,&mult,&fma);
  if(oldrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  if(oldrank==0) cout<<endl;
  myfile <<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  myfile.close();

  fftw_destroy_plan( plan );
  fftw_cleanup();
  fftw_free( data );

  MPI_Barrier( MPI_COMM_WORLD );


//    MPI_Finalize();
}



int test_fFFTWs_2d_distr_MPI_oneMatrix(long int N, int Nc, double M0, int Nfft)
{
  using namespace std;

  int oldrank;
  int oldnp;

  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Comm_rank(MPI_COMM_WORLD,&oldrank);
  MPI_Comm_size(MPI_COMM_WORLD,&oldnp);

  int Nn = ceil(N*1.0*N/M0);
  int W = 1;

  int color, newrank;

  // 0 - works
  // 1 - idle
  if( oldrank < Nn*Nc ){
    color = 0;
  }else{
    color = 1;
  }

  if( color == 0 ){
    newrank = oldrank;
  }else{
    newrank = oldrank - Nn*Nc;
  }

  MPI_Comm COMMi;

  MPI_Comm_split(MPI_COMM_WORLD, color, newrank, &COMMi);

  int myrank, np;
  MPI_Comm_rank(COMMi,&myrank);
  MPI_Comm_size(COMMi,&np);

  ofstream myfile;
  stringstream sstream;
  sstream << oldrank;
  string fnamestr = "proc" + sstream.str() + ".txt";
  myfile.open ( fnamestr.c_str() );
  myfile << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //myfile.close();

  //cout << "oldnp=" << oldnp << " oldrank=" << oldrank << " np=" << np << " rank=" << myrank << endl;
  //cout<<" np="<< np;

  //MPI_Barrier(MPI_COMM_WORLD);
  //return 0;

  if( color == 0){

  const ptrdiff_t N0 = N, N1 = N;
  fftw_plan plan;
  fftw_complex *data;
  ptrdiff_t alloc_local, local_n0, local_0_start, i, j;

  if(oldrank==0) cout<<"FFT test (fftw)"<<endl;
  if(oldrank==0) cout<<"N x N = "<<N<<" x "<<N<<endl;
  if(oldrank==0) cout<<"Nn = "<<Nn<<endl;
  myfile <<"FFT test (fftw)"<<endl;
  myfile << "N x N = "<<N<<" x "<<N<<endl;

//    MPI_Init(&argc, &argv);
  fftw_mpi_init();

  /* get local data size and allocate */
  alloc_local = fftw_mpi_local_size_2d(N0, N1, COMMi,&local_n0, &local_0_start);
  data = fftw_alloc_complex(alloc_local);

  if(oldrank==0) cout<<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  if(oldrank==0) cout<<"Memory allocated on the core 0: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Overall memory allocated for one array: "<<(sizeof(fftw_complex)*N*N)/1024.0/1024.0<<" Mb"<<endl;
  myfile <<"Memory allocated on this core: "<<(sizeof(fftw_complex)*alloc_local)/1024.0/1024.0<<" Mb"<<endl;

  /* create plan for in-place forward DFT */
  plan = fftw_mpi_plan_dft_2d(N0, N1, data, data, COMMi, FFTW_FORWARD, FFTW_ESTIMATE);

  /* initialize data to some function my_function(x,y) */
  for (i = 0; i < local_n0; ++i) for (j = 0; j < N1; ++j){
    data[i*N1 + j][0] = 1.1;//(local_0_start + i) - (j);
    data[i*N1 + j][1] = 3.1;
  }

    /* compute transforms, in-place, as many times as desired */
    //fftw_execute(plan);


  double fclock1, fclock2;
  double flastclock1, flastclock2;
  if(oldrank==0) cout<<"Forward DFT...";
  myfile<<"Forward DFT...";
  fclock1 = mclock();
  flastclock1 = mclock();
  for(int i=0;i<Nfft;i++) fftw_execute(plan);
  fclock2 = mclock();
  //MPI_Barrier(MPI_COMM_WORLD);
  flastclock2 = mclock();

  if(oldrank==0) cout<<endl;
  if(oldrank==0) cout<<"One Distributed Forward 2d DFT time: "<<diffclock(flastclock1,flastclock2)/Nfft<<"s"<<endl;
  myfile<<"One Distributed Forward 2d DFT time: "<<diffclock(fclock1,fclock2)/Nfft<<"s"<<endl;

  double add,mult,fma;
  fftw_flops(plan,&add,&mult,&fma);
  if(oldrank==0) cout<<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;
  if(oldrank==0) cout<<endl;
  myfile <<"FLOPs for one forward DFT: "<<add<<" adds, "<<mult<<" mults, "<<fma<<" fmas"<<endl;

  fftw_destroy_plan(plan);
  fftw_cleanup();
  fftw_free(data);

  }

  myfile.close();

  MPI_Barrier(MPI_COMM_WORLD);

//    MPI_Finalize();
}



int test_fFFTW_2d_NonInterval( long int N1, long int N2, long int dN ){
  using namespace std;

  int myrank, np;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
  MPI_Comm_size(MPI_COMM_WORLD,&np);

  // Hybrilit
  //double M0 = floor(110.0E9 / 16.0);
  //int Nc = 24;

  // MVS100K
  //double M0 = floor(8.0E9 / 16.0);
  //int Nc = 8;

  // MVS10P
  double M0 = floor(58.0E9 / 16.0);
  int Nc = 16;


  int W = 1;
  int Nn = np/Nc;

  if( N1 == N2) dN = 1;
  for( long int Ni = N1; Ni <= N2; Ni = Ni + dN ){
    //std::cout << "Now processing: " << i;
    if( (Ni >= sqrt(M0/2.0)) && (Ni <= sqrt(M0)) ){
      W = 1;
      if(myrank == 0) cout<<"M0="<<M0<<" Nn="<<Nn<<" Nc="<<Nc<<" W="<<W<<endl;
      if(myrank == 0) cout<<"All processes together perform only one FFT of a matrix laying completely at one node's DRAM"<<endl;
      test_fFFTW_2d_distr_MPI_clean( Ni, 1 );
    }

    if( Ni <= sqrt(M0/Nc) ){
      W = Nc;
      if(myrank == 0) cout<<"M0="<<M0<<" Nn="<<Nn<<" Nc="<<Nc<<" W="<<W<<endl;
      if(myrank == 0) cout<<"Each process performs FFT of its matrix. All processes work simultaneously. All matrices lay at one node's DRAM"<<endl;
      test_fFFTW_2d_MPI( Ni, 1 );
    }

    if( (sqrt(M0/Nc) < Ni) && (Ni < sqrt(M0/2.0)) ){
      W = floor(M0/(Ni*1.0*Ni));
      if(myrank == 0) cout<<"M0="<<M0<<" Nn="<<Nn<<" Nc="<<Nc<<" W="<<W<<endl;
      if(myrank == 0) cout<<"Several groups of processes simultaneously perform their computations. All matrices lay at one node's DRAM"<<endl;
      test_fFFTWs_2d_distr_MPI_Wgroups( Ni, Nc, M0, 1 );
//      if(myrank == 0) cout<<"DEBUG 10P: MPI_clean"<<endl;
//      test_fFFTW_2d_distr_MPI_clean( Ni, 1 );
    }

    if( Ni > sqrt(M0) ){
      W = 1;
      Nn = ceil(Ni*1.0*Ni/M0);
      if(myrank == 0) cout<<"M0="<<M0<<" Nn="<<Nn<<" Nc="<<Nc<<" W="<<W<<endl;
      if(myrank == 0) cout<<"All nodes performa FFT for one distributely storaged matrix."<<endl;
      test_fFFTWs_2d_distr_MPI_oneMatrix( Ni, Nc, M0, 1 );
    }


//    test_fFFTW_2d_distr_MPI_clean( N1 );

    MPI_Barrier(MPI_COMM_WORLD);
  }
  return 0;
}



int conv_K( int i, int j, fftw_complex a ){
  a[0] = i; //1./(i+1);
  a[1] = 1.0; //2./(j+1);
}

int conv_h( int i, int j, fftw_complex a ){
  a[0] = i;
  a[1] = j*j;
}

int print_matrix( fftw_complex* A, int N, char* mname ){
  using namespace std;

  cout << endl << "Matrix " << mname << ":" << endl;
  for (int i = 0; i < N; i++){
    for (int j = 0; j < N; j++){
      cout << A[i*N+j][0] << "+i" << A[i*N+j][1] << " ";
    }
    cout << endl;
  }

}


int convolution_FFT( fftw_complex* A, fftw_complex* K, fftw_complex* B, int N, int M ){
  using namespace std;

  int myrank;

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  fftw_complex *x, *y, *z;
  int Nmax = 10;
  int Nfft = 1;

  fftw_plan forward_plan1;
  fftw_plan forward_plan2;
  fftw_plan backward_plan;

  double fclock1, fclock2;
  double bclock1, bclock2;
  double mclock1, mclock2;

  M = M+6;

  x = fftw_alloc_complex((N+M-1)*(N+M-1));
  y = fftw_alloc_complex((N+M-1)*(N+M-1));
  z = fftw_alloc_complex((N+M-1)*(N+M-1));

  forward_plan1 = fftw_plan_dft_2d( (N+M-1),(N+M-1), x,x, FFTW_FORWARD,FFTW_MEASURE );
  forward_plan2 = fftw_plan_dft_2d( (N+M-1),(N+M-1), y,y, FFTW_FORWARD,FFTW_MEASURE );
  backward_plan = fftw_plan_dft_2d( (N+M-1),(N+M-1), y,z, FFTW_BACKWARD,FFTW_MEASURE );

  int shift = 0;
  for(int i=0;i<(N+M-1);i++){
    for(int j=0;j<(N+M-1);j++){
      if( ((i-shift) < N)&&((j-shift) < N)&&((i-shift) >= 0)&&((j-shift) >= 0) ){
        x[i*(N+M-1)+j][0] = A[((i-shift))*N+((j-shift))][0];
        x[i*(N+M-1)+j][1] = A[((i-shift))*N+((j-shift))][1];
      }else{
        x[i*(N+M-1)+j][0] = 0.0;
        x[i*(N+M-1)+j][1] = 0.0;
      }
    }
  }

  print_matrix( x, M+N-1, "x" );


  for(int i=0;i<(N+M-1)*(N+M-1);i++){
    y[i][0] = K[i][0];
    y[i][1] = K[i][1];
  }

  // Forward

  if(myrank==0) cout<<endl<<"Forward DFT of A...";
  fftw_execute(forward_plan1);

  if(myrank==0) cout<<endl<<"Forward DFT of K...";
  fftw_execute(forward_plan2);

  if(myrank==0) cout<<endl<<"Piece-wise multiplication F(A)*F(K)";
  fftw_complex t;
  for(int i=0;i<(N+M-1)*(N+M-1);i++){
    t[0] = x[i][0]*y[i][0] - x[i][1]*y[i][1];
    t[1] = x[i][1]*y[i][0] + x[i][0]*y[i][1];
    y[i][0] = t[0]/(M+N-1)/(M+N-1);
    y[i][1] = t[1]/(M+N-1)/(M+N-1);
  }

  if(myrank==0) cout<<endl<<"Backward DFT of F(A)*F(K)...";
  fftw_execute(backward_plan);

  print_matrix( z, M+N-1, "Bfft" );

  fftw_destroy_plan(forward_plan1);
  fftw_destroy_plan(forward_plan2);
  fftw_destroy_plan(backward_plan);
  fftw_cleanup();
  //fftw_free(x_in);
  fftw_free(x);
  fftw_free(y);
  fftw_free(z);



  for (int i = 0; i < M; i++){
    for (int j = 0; j < M; j++){
      fftw_complex a;

      a[0] = 0.0;
      a[1] = 0.0;
      for (int n = 0; n < N; n++){
        for (int l = 0; l < N; l++){
          a[0] = a[0] + A[n*N+l][0]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][0] - A[n*N+l][1]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][1];
          a[1] = a[1] + A[n*N+l][0]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][1] + A[n*N+l][1]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][0];
        }
      }

      B[i*M+j][0] = a[0];
      B[i*M+j][1] = a[1];
    }
  }

  return 0;
}


int convolution_direct( fftw_complex* A, fftw_complex* K, fftw_complex* B, int N, int M ){

  for (int i = 0; i < M; i++){
    for (int j = 0; j < M; j++){
      fftw_complex a;

      a[0] = 0.0;
      a[1] = 0.0;
      for (int n = 0; n < N; n++){
        for (int l = 0; l < N; l++){
          a[0] = a[0] + A[n*N+l][0]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][0] - A[n*N+l][1]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][1];
          a[1] = a[1] + A[n*N+l][0]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][1] + A[n*N+l][1]*K[(n-i+M-1)*(N+M-1)+(l-j+M-1)][0];
        }
      }

      B[i*M+j][0] = a[0];
      B[i*M+j][1] = a[1];
    }
  }

  return 0;
}

int convolution_2d(int gN){

  int N = 4;
  int M = 2;

  fftw_complex *A,*B,*K;

  A = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*N*N);
  B = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*M*M);
  K = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*(N+M-1)*(N+M-1));

  for (int i = 0; i < N; i++){
    for (int j = 0; j < N; j++){
      fftw_complex a;
      conv_h( i, j, a );
      A[i*N+j][0] = a[0];
      A[i*N+j][1] = a[1];
    }
  }
  print_matrix( A, N, "A" );

  for (int i = 0; i < N+M-1; i++){
    for (int j = 0; j < N+M-1; j++){
      fftw_complex a;
      conv_K( i, j, a );
      K[i*(N+M-1)+j][0] = a[0];
      K[i*(N+M-1)+j][1] = a[1];
    }
  }
  print_matrix( K, N+M-1, "K" );

  convolution_direct( A, K, B, N, M );
  print_matrix( B, M, "B" );

  convolution_FFT( A, K, B, N, M );
  print_matrix( B, M, "K" );


}

int optimizeConvParameter( int N, int M, int r, char* fname ){
  using namespace std;

  int m = 1;

  std::fstream myfile(fname, std::ios_base::in);

  double curTime;
  int curN;

  int nLines = 0;
  cout << endl << "Loaded data:" << endl;
  while ( (myfile >> curN)&&(myfile >> curTime) ){

    cout << curN << " " << curTime << endl;
    nLines++;

  }
  cout << endl;

  std::fstream myfile1(fname, std::ios_base::in);

  double times[nLines];
  int Ns[nLines];

  int i = 0;
  while ( (myfile1 >> Ns[i])&&(myfile1 >> times[i]) ){
    i++;
  }

  bool found = false;
  double curFFTtime = 0.0;
  double apptime = 0.0;
  int curFFT_N = 0;
  double tmin;
  int mmin = 1;
  for( m = 1; m <= M/N*r; m++){
    cout << "m = " << m << ";  N/r + M/m = " << N/r + M/m;
    curFFT_N = N/r + M/m;

    found = false;
    for ( int Nm = 0; Nm < nLines-1; Nm++ ){
      int Np = Nm + 1;
      if( ( curFFT_N > Ns[Nm] )&&( curFFT_N <= Ns[Np] ) ){
        apptime = (times[Np]-times[Nm])/(Ns[Np]-Ns[Nm])*(curFFT_N-Ns[Nm]) + times[Nm];
        found = true;
      }
    }
    if( !found ){
      if ( curFFT_N > Ns[nLines-1] ){
        int Np = nLines-1;
        int Nm = nLines-2;
        apptime = (times[Np]-times[Nm])/(Ns[Np]-Ns[Nm])*(curFFT_N-Ns[Np]) + times[Np];
      }else{
        int Np = 1;
        int Nm = 0;
        apptime = (times[Np]-times[Nm])/(Ns[Np]-Ns[Nm])*(curFFT_N-Ns[Nm]) + times[Nm];
      }

    }

    cout << "; apptime = " << apptime << endl;

    //double tm = (m+r*M/N)*(m+r*M/N)/((1/r+M/N)*(1/r+M/N))*apptime;
    double tm = m*m*r*r*((1.0*N/r+1.0*M/m)*(1.0*N/r+1.0*M/m))/((M+N)*(M+N))*apptime;

    cout << "m = "<< m << " tm = " << tm << endl;

    if( m == 1 ) tmin = tm;
    if( tm < tmin ){
      mmin = m;
      tmin = tm;
    }

  }

  cout << "m_min = " << mmin << " t_min = " << tmin << endl;

  return mmin;
}

int main(int argc, char **argv){
  using namespace std;

  int myrank, np;

  MPI_Init(&argc, &argv);
  //fftw_mpi_init();

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
  MPI_Comm_size(MPI_COMM_WORLD,&np);

  if (myrank == 0) cout << "np = " << np;

  double_complex *x,*x_in;
  long int N=2*1024*1024, Nmax=10;
  long int N1 = N;
  long int N2 = N;
  long int dN = 0;

  // In
  //cout<<"Input N: ";
  //cin>>N;
  if(argc > 1){
    N = atoi(argv[1]);
  }else{
    N = 100;
  }
  N1 = N;
  N2 = N;
  dN = 0;

  if(argc == 3){
    N1 = atoi(argv[1]);
    N2 = atoi(argv[2]);
    N = N1;
    dN = N2 - N1;
  }

  if(argc == 4){
    N1 = atoi(argv[1]);
    N2 = atoi(argv[2]);
    dN = atoi(argv[3]);
    N = N1;
  }

  if (myrank == 0) cout<<endl<<"N1="<<N1<<" N2="<<N2<<" dN="<<dN<<endl;
  if (myrank == 0) cout << endl;

  double re, im;

  double aclock1,aclock2;
  aclock1 = mclock();

  //test_MKL_1d(N);
  //test_FFTW_1d_MPI( N );
  //test_fRW( N );
  //test_1000writes( N, Ncount );
  //test_1000reads( N, Ncount );
  //test_fFFTW_bFFTW_mult_2d_MPI( N );
  //test_fFFTW_2d_MPI( N );
  //test_mult_2d_MPI( N );

  test_fFFTW_2d_distr_MPI( N );
  //test_fFFTW_2d_NonInterval( N1, N2, dN );

  //convolution_2d( N );
  //optimizeConvParameter( 10000, 10000, 10, "allres-mvs100k.txt");

  //int_darbu_1d( 100.0, N, &re, &im );


  aclock2 = mclock();
  if(myrank==0) cout<<"Total time: "<<diffclock(aclock1,aclock2)<<"s"<<endl;

  //if(myrank==0) printf("%.6lf seconds elapsed\n", t2-t1);

  MPI_Finalize();

}
