HOMEDIR=$(HOME)

MPILIBS= 
#-L$(HOMEDIR)/mpich2-install/lib
#FFTWLIBS=-L$(HOMEDIR)/lib -L$(HOMEDIR)/soft/fftw/lib

#
# CHOOSE IF YOU ARE SUING MVS-10P
#
#FFTWLIBS=-L$(HOMEDIR)/soft/fftw/lib
FFTWLIBS=-L$(HOMEDIR)/soft/fftw-10p/lib

LIBS=$(MPILIBS) $(FFTWLIBS) -L/usr/local/lib/


MPIINCLUDE=
#-I$(HOMEDIR)/mpich2-install/include -DMPICH_IGNORE_CXX_SEEK
#FFTWINCLUDE=-I$(HOMEDIR)/include -I$(HOMEDIR)/soft/fftw/include -DMPICH_IGNORE_CXX_SEEK

#
# CHOOSE IF YOU ARE SUING MVS-10P
#
#FFTWINCLUDE=-I$(HOMEDIR)/soft/fftw/include -DMPICH_IGNORE_CXX_SEEK
FFTWINCLUDE=-I$(HOMEDIR)/soft/fftw-10p/include -I/usr/local/include/ -DMPICH_IGNORE_CXX_SEEK

MKLINCLUDE=-I/opt/intel/cmkl/10.1.0.015/include

INCLUDES=$(MPIINCLUDE) $(FFTWINCLUDE) $(MKLINCLUDE)

